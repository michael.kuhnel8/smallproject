# Small Project

## Description

- This is a small semester project for the course SSP.
- The project demonstrates generating a HTML pages from markdown files and deploying it to a gitlab pages.

## Installation

- Install Python 3.8

- Install virtualenv environment

```bash
pip install virtualenv
```

## Setup

- Create a virtual environment and activate it

```bash
virtualenv venv
source venv/bin/activate
```