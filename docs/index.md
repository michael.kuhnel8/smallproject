# Small Project to SSP

This is a small semester project for the course SSP.

## Installation

- To install the project, clone the repository and run the following command:

```bash
pip install -r requirements.txt
```

## Build

- To build the project, run the following command:

```bash
mkdocs build
```
